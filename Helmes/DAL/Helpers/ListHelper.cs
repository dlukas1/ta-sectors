﻿using Helmes.Models;
using System.Collections.Generic;
using System.Linq;

namespace Helmes.DAL.Helpers
{
    public class ListHelper
    {
        private List<Sector> sortedSectors = new List<Sector>();
        private List<Sector> allSectors;
        public List<Sector> SectorSort(List<Sector> sectors)
        {
            allSectors = sectors;
            var BaseSectors = sectors.Where(x => x.ParentSector == null);
            foreach (var s in BaseSectors)
            {
                AddKids(s);
            }
            return sortedSectors;
        }

        private void AddKids(Sector s)
        {
            sortedSectors.Add(s);
            var subSektors = allSectors.Where(x => x.ParentSector != null && x.ParentSector == s).OrderBy(x => x.Name);
            if (subSektors != null)
                foreach (var ss in subSektors)
                {
                    AddKids(ss);
                }
        }
    }
}
