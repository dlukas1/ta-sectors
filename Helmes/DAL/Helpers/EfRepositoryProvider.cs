﻿using Helmes.Interfaces;
using Helmes.Repositories;
using System;
using System.Collections.Generic;

namespace Helmes.DAL.Helpers
{
    public class EfRepositoryProvider : IRepositoryProvider
    {
        private readonly IDataContext _dataContext;
        private readonly Dictionary<Type, object> RepositoryCache =
            new Dictionary<Type, object>();

        public EfRepositoryProvider(IDataContext context)
        {
            _dataContext = context;
        }

        private static readonly Dictionary<Type, Func<IDataContext, object>> DictionaryOfRepoCreationFuncs =
            new Dictionary<Type, Func<IDataContext, object>>()
            {
                {typeof(IUserRepo), dataContext => new UserRepository( dataContext) },
                {typeof(ISectorRepo), dataContext => new SectorRepository(dataContext) },
                {typeof(IUsersSectorsRepo), dataContext => new UsersSectorsRepository(dataContext) }
            };


        public TRepository GetCustomRepository<TRepository>()
        {
            return GetOrMakeRepository<TRepository>();
        }

        public IRepository<T> GetStandartRepository<T>() where T : class
        {
            return GetOrMakeRepository<IRepository<T>>(dataContext => 
            new EfRepository<T>(dataContext));
        }
        private TRepository GetOrMakeRepository<TRepository>
            (Func<IDataContext, object> repoCreationFunction = null)
        {
            object repo;
            RepositoryCache.TryGetValue(typeof(TRepository), value: out repo);
            if (repo != null)
            {   //if repo exist return it
                return (TRepository)repo;
            }
            if (repoCreationFunction == null)
            {
                DictionaryOfRepoCreationFuncs.
                    TryGetValue(typeof(TRepository), out repoCreationFunction);
            }
            if (repoCreationFunction == null)
            {
                throw new ArgumentNullException($"No factory found for repository type {typeof(TRepository).FullName}");
            }
            //create a repo
            repo = repoCreationFunction(arg: _dataContext);
            //save repo to cache
            RepositoryCache[key: typeof(TRepository)] = repo;
            //return repo
            return (TRepository)repo;
        }

        }
    }
