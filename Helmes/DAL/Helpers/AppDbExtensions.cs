﻿using System.Collections.Generic;
using System.Linq;
using Helmes.Models;

namespace Helmes.DAL.Helpers
{
    public static class AppDbExtensions
    {
        public static void EnsureSeedData()
        {
            var ctx = new AppDbContext();

            if (!ctx.Sectors.Any())
            {                
                // Base sectors:
                Sector Manufactoring = new Sector("Manufactoring");
                Sector Other = new Sector("Other");
                Sector Services = new Sector("Services");
                var BaseSectors = new List<Sector>
                {
                    Manufactoring, Other, Services 
                };
                foreach (var s in BaseSectors)
                {
                    ctx.Sectors.Add(s);
                }

                // Manufactoring Sub-Sectors:
                Sector ConstructionMaterials = new Sector("Construction materials");
                Sector ElectronicsAndOptics = new Sector("Electronics and Optics");
                Sector FoodAndBewerage = new Sector("Food and Beverage");
                Sector Furniture = new Sector("Furniture");
                Sector Machinery = new Sector("Machinery");
                Sector MetalWorking = new Sector("MetalWorking");
                Sector PlasticAndRubber = new Sector("Plastic And Rubber");
                Sector Printing = new Sector("Printing");
                Sector TextileAndClothing = new Sector("Textile and Clothing");
                Sector Wood = new Sector("Wood");
                var ManufactoringSubSectors = new List<Sector>
                {
                    ConstructionMaterials, ElectronicsAndOptics, FoodAndBewerage, Furniture,
                    Machinery, MetalWorking, PlasticAndRubber, Printing, TextileAndClothing, Wood
                };
                foreach (var s in ManufactoringSubSectors)
                {
                    s.ParentSector = Manufactoring;
                    ctx.Sectors.Add(s);
                }

                // Other Sub-Sectors
                Sector CreativeIndustry = new Sector("Creative Industry");
                Sector EnergyTechnology = new Sector("Energy Technology");
                Sector Environment = new Sector("Environment");
                var OtherSubSectors = new List<Sector>
                {
                    CreativeIndustry, EnergyTechnology, Environment
                };
                foreach (var s in OtherSubSectors)
                {
                    s.ParentSector = Other;
                    ctx.Sectors.Add(s);
                }

                // Services Sub-Sectors
                Sector BusinessServices = new Sector("Business services");
                Sector Engineering = new Sector("Engineering");
                Sector IT = new Sector("Information Technology and Telecommunications");
                Sector Tourism = new Sector("Tourism");
                Sector TranslationServices = new Sector("Translation services");
                Sector TransportAndLogistics = new Sector("Transport and Logistics");
                var ServicesSubSectors = new List<Sector>
                    {
                        BusinessServices, Engineering, IT, Tourism, TranslationServices, TransportAndLogistics
                    };
                foreach (var s in ServicesSubSectors)
                {
                    s.ParentSector = Services;
                    ctx.Sectors.Add(s);
                }

                // Food And Bewerage Sub-Sectors:
                Sector Bakery = new Sector("Bakery and Confectionery products");
                Sector Beverages = new Sector("Beverages");
                Sector Fish = new Sector("Fish and Fish Products");
                Sector Meat = new Sector("Meat and Meat Products");
                Sector Milk = new Sector("Milk and Diary Products");
                Sector FoodOther = new Sector("Other(Food And Bewerage)");
                Sector Snacks = new Sector("Sweets & Snack Food");
                var FoodAndBewerageSubSectors = new List<Sector>
                    {
                        Bakery, Beverages, Fish, Meat, Milk, FoodOther, Snacks
                    };
                foreach (var s in FoodAndBewerageSubSectors)
                {
                    s.ParentSector = FoodAndBewerage;
                    ctx.Sectors.Add(s);
                }

                // Furniture Sub-Sectors
                Sector Bath = new Sector("Bathroom / Sauna");
                Sector Bed = new Sector("Bedroom");
                Sector Kids = new Sector("Children's room");
                Sector Kitchen = new Sector("Kitchen");
                Sector LivingRoom = new Sector("Living room");
                Sector Office = new Sector("Office");
                Sector OtherFurniture = new Sector("Other (Furniture)");
                Sector Outdoor = new Sector("Outdoor");
                Sector ProjFurniture = new Sector("Project furniture");
                var FurnitureSubSectors = new List<Sector>
                {
                    Bath, Bed, Kids, Kitchen, LivingRoom, Office, 
                    OtherFurniture, Outdoor, ProjFurniture
                };
                foreach (var s in FurnitureSubSectors)
                {
                    s.ParentSector = Furniture;
                    ctx.Sectors.Add(s);
                }

                // Machinrey Sub-Sectors:
                Sector MachineryComponents = new Sector("Machinery components");
                Sector MachineryEquipment = new Sector("Machinery equipment/tools");
                Sector ManufactureOfMachinery = new Sector("Manufacture of machinery");
                Sector Maritime = new Sector("Maritime");
                Sector MetalStructures = new Sector("Metal Structures");
                Sector MachineryOther = new Sector("Other (Machinery)");
                Sector RepairAndMaintenance = new Sector("Repair and maintenance service");
                var MachinerySubSectors = new List<Sector>
                {
                    MachineryComponents, MachineryEquipment, ManufactureOfMachinery, Maritime,
                    MetalStructures, MachineryOther, RepairAndMaintenance
                };
                foreach (var s in MachinerySubSectors)
                {
                    s.ParentSector = Machinery;
                    ctx.Sectors.Add(s);
                }

                // Maritime Sub-Sectors:
                Sector WorkBoats = new Sector("Aluminium and steel workboats");
                Sector BoatBoulding = new Sector("Boat/Yacht building");
                Sector ShipRepair = new Sector("Ship repair and conversion");
                var MaritimeSubSectors = new List<Sector>
                {
                    WorkBoats, BoatBoulding, ShipRepair
                };
                foreach (var s in MaritimeSubSectors)
                {
                    s.ParentSector = Maritime;
                    ctx.Sectors.Add(s);
                };

                // Metal Working Sub-Sectors:
                Sector MetalConstruction = new Sector("Construction of metal structures");
                Sector MetalHouses = new Sector("Houses and buildings");
                Sector MetalProducts = new Sector("Metal products");
                Sector MetalWorks = new Sector("Metal works");
                var MetalWorkingSubSectors = new List<Sector>
                {
                    MetalConstruction, MetalHouses, MetalProducts, MetalWorks
                };
                foreach (var s in MetalWorkingSubSectors)
                {
                    s.ParentSector = MetalWorking;
                    ctx.Sectors.Add(s);
                }

                // Metal Works Sub-Sectors:
                Sector CNC = new Sector("CNC-machining");
                Sector ForgingsFasteners = new Sector("Forgings, Fasteners");
                Sector Cutting = new Sector("Gas, Plazma, Lazer Cutting");
                Sector Welding = new Sector("MIG, TIG, Aluminium welding");
                var MetalWorksSubSector = new List<Sector>
                {
                    CNC, ForgingsFasteners, Cutting, Welding
                };
                foreach (var s in MetalWorksSubSector)
                {
                    s.ParentSector = MetalWorks;
                    ctx.Sectors.Add(s);
                }

                // Plastic and Rubber Sub-Sectors:
                Sector PlastickPackage = new Sector("Packaging");
                Sector PlasticGoods = new Sector("Plastic goods");
                Sector PlasticProcessing = new Sector("Plastic processing technology");
                Sector PlasticProfiles = new Sector("Plastic profiles");
                var PlasticAndRubberSubSectors = new List<Sector>
                {
                    PlastickPackage, PlasticGoods, PlasticProcessing, PlasticProfiles
                };
                foreach (var s in PlasticAndRubberSubSectors)
                {
                    s.ParentSector = PlasticAndRubber;
                    ctx.Sectors.Add(s);
                }

                // Plastic Processing Sub-Sectors:
                Sector Blowing = new Sector("Blowing");
                Sector Moulding = new Sector("Moulding");
                Sector PlasticWelding = new Sector("Plastic welding and processing");
                var PlastingProcessingTechnologySubSectors = new List<Sector>
                {
                    Blowing, Moulding, PlasticWelding
                };
                foreach (var s in PlastingProcessingTechnologySubSectors)
                {
                    s.ParentSector = PlasticProcessing;
                    ctx.Sectors.Add(s);
                }

                // Printing Sub-Sectors:
                Sector Advertising = new Sector("Advertising");
                Sector Books = new Sector("Book/Periodical printing");
                Sector Labeling = new Sector("Labelling and packaging printing");
                var PrintingSubSectors = new List<Sector>
                {
                    Advertising, Books, Labeling
                };
                foreach (var s in PrintingSubSectors)
                {
                    s.ParentSector = Printing;
                    ctx.Sectors.Add(s);
                }

                // Textile Sub-Sectors: 
                Sector Cloth = new Sector("Clothing");
                Sector Textile = new Sector("Textile");
                var TextileClothingSubSectors = new List<Sector>
                {
                    Cloth, Textile
                };
                foreach (var s in TextileClothingSubSectors)
                {
                    s.ParentSector = TextileAndClothing;
                    ctx.Sectors.Add(s);
                }

                // Wood Sub-Sectors:
                Sector WoodOther = new Sector("Other(Wood)");
                Sector WoodMaterials = new Sector("Wooden building materials");
                Sector WoodHouses = new Sector("Wooden houses");
                var WoodSubSectors = new List<Sector>
                {
                    WoodOther, WoodMaterials, WoodHouses
                };
                foreach (var s in WoodSubSectors)
                {
                    s.ParentSector = Wood;
                    ctx.Sectors.Add(s);
                }

                // IT Sub-Sectors:
                Sector DataWebE = new Sector("Data processing, Web portals, E-marketing");
                Sector Programming = new Sector("Programming, Consultancy");
                Sector SoftHardWare = new Sector("Software, Hardware");
                Sector TV = new Sector("Telecommunications");
                var ITSubSectors = new List<Sector>
                {
                    DataWebE, Programming, SoftHardWare, TV
                };
                foreach (var s in ITSubSectors)
                {
                    s.ParentSector = IT;
                    ctx.Sectors.Add(s);
                }

                // Transport and Logistics Sub-Sectors:
                Sector AirTransport = new Sector("Air");
                Sector RailTransport = new Sector("Rail");
                Sector RoadTransport = new Sector("Road");
                Sector WaterTransport = new Sector("Water");
                var TransportLogisticsSubSector = new List<Sector>
                {
                    AirTransport, RailTransport, RoadTransport, WaterTransport
                };
                foreach (var s in TransportLogisticsSubSector)
                {
                    s.ParentSector = TransportAndLogistics;
                    ctx.Sectors.Add(s);
                }

                ctx.SaveChanges();
            }
        }
    }
}