﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Helmes.DAL.Helpers
{
    public static class SessionExtensions
    {
        // Set() will set data to session
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value,Formatting.Indented,
                new JsonSerializerSettings
{
    PreserveReferencesHandling = PreserveReferencesHandling.Objects
}));
        }

        // Get() will get data from session
        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
