﻿using Helmes.Interfaces;
using Helmes.Models;
using Helmes.Repositories;
using System;

namespace Helmes.DAL
{
    public class UOW : IUOW
    {
        private readonly IDataContext _dataContext;
        private readonly IRepositoryProvider _repositoryProvider;
        public UOW(IDataContext context, IRepositoryProvider provider)
        {
            _repositoryProvider = provider;
            _dataContext = context ?? throw new ArgumentNullException(
                nameof(context));
        }
        public int SaveChanges()
        {
            return ((AppDbContext)_dataContext).SaveChanges();
        }


        public IRepository<User> Users => new EfRepository<User>(_dataContext);
        public IRepository<Sector> Sectors => new EfRepository<Sector>(_dataContext);
        public IRepository<UsersSectors> UsersSectors => new EfRepository<UsersSectors>(_dataContext);


    }
}
