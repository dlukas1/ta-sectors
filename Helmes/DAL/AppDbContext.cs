﻿using Helmes.Interfaces;
using Helmes.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Helmes.DAL
{
    public class AppDbContext : DbContext, IDataContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Sector> Sectors { get; set; }
        public DbSet<UsersSectors> UsersSectors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server = (localdb)\mssqllocaldb; 
            Database = HelmesDb; Trusted_Connection = True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Prevent cascade delete
            foreach (var relationship in modelBuilder.Model.GetEntityTypes()
                .SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
                //now when trying to remove any entity will get an exception
                //it’s possible only if you manually delete all related entries
            }
            base.OnModelCreating(modelBuilder);

        }
    }
}
