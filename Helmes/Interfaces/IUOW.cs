﻿using Helmes.Models;

namespace Helmes.Interfaces
{
    // Will provide atomic save to db
    public interface IUOW
    {
        int SaveChanges(); // number of changes made
        IRepository<User> Users { get; }
        IRepository<Sector> Sectors { get; }
        IRepository<UsersSectors> UsersSectors { get; }
    }
}
