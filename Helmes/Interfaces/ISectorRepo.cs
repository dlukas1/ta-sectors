﻿using Helmes.Models;

namespace Helmes.Interfaces
{
    public interface ISectorRepo : IRepository<Sector>
    {
    }
}
