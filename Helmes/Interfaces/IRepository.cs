﻿using System.Collections.Generic;


namespace Helmes.Interfaces
{
    public interface IRepository<T>
    {
        List<T> All { get; }
        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(int id);
        void Delete(T entity);
        int SaveChanges();
    }

}
