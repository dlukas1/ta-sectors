﻿using Helmes.ViewModels;

namespace Helmes.Interfaces.IServices
{
    public interface IUserFormViewModelServices
    {
        UserFormViewModel GetViewModel();
        void SaveFormData(UserFormViewModel userForm);
    }
}
