﻿using Helmes.ViewModels;

namespace Helmes.Interfaces.IServices
{
    public interface IUsersSectorsServices
    {
        void SaveUsersSectors(UserFormViewModel userForm);
    }
}
