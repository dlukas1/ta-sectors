﻿using Helmes.Models;

namespace Helmes.Interfaces
{
    public interface IUsersSectorsRepo : IRepository<UsersSectors>
    {
    }
}
