﻿using Helmes.Models;

namespace Helmes.Interfaces
{
    public interface IUserRepo : IRepository<User>
    {
    }
}
