﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Helmes.Interfaces
{
    public interface IRepositoryProvider
    {
        TRepository GetCustomRepository<TRepository>();
        IRepository<T> GetStandartRepository<T>() where T : class;
    }
}
