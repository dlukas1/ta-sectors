﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Helmes.Models;
using Helmes.ViewModels;
using Helmes.DAL.Helpers;
using Helmes.Interfaces.IServices;

namespace Helmes.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserFormViewModelServices _formVmServices;
        private UserFormViewModel viewModel;
        public HomeController(IUserFormViewModelServices formVmServices)
        {
            _formVmServices = formVmServices;
            

        }
        public IActionResult Index()
        {
            viewModel = _formVmServices.GetViewModel();
            
            if (HttpContext.Session.Keys.Contains("model"))
            {
                // Get data from session and append to ViewModel
                UserFormViewModel sessionModel = HttpContext.Session.Get<UserFormViewModel>("model");
                viewModel.UserId = sessionModel.UserId;
                viewModel.UserName = sessionModel.UserName;
                viewModel.SelectedSectors = sessionModel.SelectedSectors;
                viewModel.AgreeToTerms = sessionModel.AgreeToTerms;
            }                       
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Save(UserFormViewModel model)
        {           
            if (!ModelState.IsValid)
            {
                // Repopulate ViewModel
                UserFormViewModel VM = new UserFormViewModel();
                VM = _formVmServices.GetViewModel();
                return View("Index", VM);
            }

            _formVmServices.SaveFormData(model);

            // Set UserFormViewModel to session
            HttpContext.Session.Set("model", model);
            return RedirectToAction("Index");
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
