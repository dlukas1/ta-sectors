﻿using Helmes.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Helmes.ViewModels
{
    public class UserFormViewModel
    {
        public int UserId { get; set; }
        [Required(ErrorMessage ="Name is required")]
        public string UserName { get; set; }
        public SelectList Sectors { get; set; } // To display all sectors
        [Required(ErrorMessage = "Select at least 1 sector")]
        public int[] SelectedSectors { get; set; } // To store selected sectors id's
        [Required]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Agree to terms")] 
        public bool AgreeToTerms { get; set; }
    }
}
