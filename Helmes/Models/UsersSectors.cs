﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Helmes.Models
{
    public class UsersSectors
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        [Required]
        public int SectorId { get; set; }
        public virtual Sector Sector { get; set; }

    }
}
