﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Helmes.Models
{
    public class Sector
    {
        public Sector(string n)
        {
            Name = n;
        }
        public Sector() { }
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public Sector ParentSector { get; set; }
        public virtual List<UsersSectors> UsersSectors { get; set; } = new List<UsersSectors>();

        public Sector GetParent()
        {
            return ParentSector;
        }

        int level = 0;
        public string Indentate(Sector s)
        {            
            Sector temp = s;
            while (temp.ParentSector != null)
            {
                level++;
                temp = temp.ParentSector;
                Indentate(temp);
            }
             return new string('.', level*2) + s.Name;
        }
    }
}
