﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Helmes.Models
{
    public class User
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public bool AgreeToTerms { get; set; }
        public List<UsersSectors> UsersSectors { get; set; }

    }
}
