﻿using Helmes.DAL.Helpers;
using Helmes.Interfaces;
using Helmes.Interfaces.IServices;
using Helmes.Models;
using Helmes.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace Helmes.Services
{
    public class UserFormViewModelServices : IUserFormViewModelServices
    {
        private readonly IUOW _uow;
        private readonly IUsersSectorsServices _usersSectorsServices;

        public UserFormViewModelServices(IUOW uow)
        {
            _uow = uow;
            _usersSectorsServices = new UsersSectorsServices(uow);
        }
        public UserFormViewModel GetViewModel()
        {

            var allSectors = _uow.Sectors.All;
            ListHelper helper = new ListHelper();
            allSectors = helper.SectorSort(allSectors);
            foreach (var s in allSectors)
            {
                s.Name = s.Indentate(s);
            }
            return new UserFormViewModel
            {
                Sectors = new SelectList(allSectors, "Id", "Name")
            };
        }

        public void SaveFormData(UserFormViewModel userForm)
        {
            // if userform have userID - update, if not - create new
            if (userForm.UserId == 0)
            {
                AddNewFormData(userForm);
            }
            else
            {
                UpdateFormData(userForm);
            }
            
        }

        public void UpdateFormData(UserFormViewModel userForm)
        {
            var user = _uow.Users.GetById(userForm.UserId);
            user.Name = userForm.UserName;
            user.AgreeToTerms = userForm.AgreeToTerms;
            _uow.Users.Update(user);

            // Get UsersSectors by userId
            var oldSectors = _uow.UsersSectors.All.Where(x => x.UserId == userForm.UserId);

            // Delete old sectors
            foreach (var sector in oldSectors)
            {
                _uow.UsersSectors.Delete(sector.Id);
            }

            // Add new sectors
            _usersSectorsServices.SaveUsersSectors(userForm);
            _uow.SaveChanges();
        }

        public void AddNewFormData(UserFormViewModel userForm)
        {
            User user = new User { Name = userForm.UserName, AgreeToTerms = userForm.AgreeToTerms };
            _uow.Users.Add(user);
            _uow.SaveChanges(); // After SaveChanges() user has id from db
            userForm.UserId = user.Id;
            _usersSectorsServices.SaveUsersSectors(userForm);
            _uow.SaveChanges();
        }
    }
}
