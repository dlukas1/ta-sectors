﻿using Helmes.Interfaces;
using Helmes.Interfaces.IServices;
using Helmes.Models;
using Helmes.ViewModels;

namespace Helmes.Services
{
    public class UsersSectorsServices : IUsersSectorsServices
    {
        private readonly IUOW _uow;
        public UsersSectorsServices(IUOW uow)
        {
            _uow = uow;
        }
        public void SaveUsersSectors(UserFormViewModel userForm)
        {
            foreach (var sector in userForm.SelectedSectors)
            {
                UsersSectors usersSector = new UsersSectors() { UserId = userForm.UserId, SectorId = sector };
                _uow.UsersSectors.Add(usersSector);
            }
            _uow.SaveChanges();
        }
    }
}
