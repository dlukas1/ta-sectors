﻿using Helmes.Interfaces;
using Helmes.Models;
using Microsoft.EntityFrameworkCore;

namespace Helmes.Repositories
{
    public class UserRepository : EfRepository<User>, IUserRepo
    {
        public UserRepository(IDataContext context) : base(context) { }
    }
}
