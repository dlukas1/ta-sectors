﻿using Helmes.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


// All LINQ queries should be here 


namespace Helmes.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : class
    {
        protected DbContext RepositoryDbContext;
        protected DbSet<T> RepositoryDbSet;
        public EfRepository(IDataContext dataContext)
        {
            RepositoryDbContext = dataContext as DbContext?? 
                throw new ArgumentNullException("NO EF DB CONTEXT");
            RepositoryDbSet = RepositoryDbContext.Set<T>();
        }

        public List<T> All
        {
            get { return RepositoryDbSet.ToList(); }
        }

        public void Add(T entity)
        {
            RepositoryDbSet.Add(entity);
        }

        public void AddMany(List<T> entities)
        {
            foreach (var item in entities)
            {
                RepositoryDbSet.Add(item);
            }
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            Delete(entity);
        }

        public void Delete(T entity)
        {
            RepositoryDbSet.Remove(entity);
        }

        public T GetById(int id)
        {
            return RepositoryDbSet.Find(id);
        }

        public int SaveChanges()
        {
            return RepositoryDbContext.SaveChanges();
        }

        public void Update(T entity)
        {
            RepositoryDbSet.Update(entity);
        }

        // ASYNC methods
        public async Task<IEnumerable<T>> AllAsync() =>
            await RepositoryDbSet.ToListAsync();

        public async Task AddAsync(T entity)
        {
            if (entity == null) throw new InvalidOperationException(message: "Can't add null");
            await RepositoryDbSet.AddAsync(entity: entity);
        }

        public Task<T> FindAsync(params object[] id) => 
            RepositoryDbSet.FindAsync(keyValues: id);

        
    }
}
