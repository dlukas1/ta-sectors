﻿using Helmes.Interfaces;
using Helmes.Models;
using Microsoft.EntityFrameworkCore;

namespace Helmes.Repositories
{
    public class UsersSectorsRepository : EfRepository<UsersSectors>, IUsersSectorsRepo
    {
        public UsersSectorsRepository(IDataContext context) : base(context) { }
    }
}
