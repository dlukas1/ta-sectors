﻿using Helmes.Interfaces;
using Helmes.Models;
using Microsoft.EntityFrameworkCore;

namespace Helmes.Repositories
{
    public class SectorRepository : EfRepository<Sector>, ISectorRepo
    {
        public SectorRepository (IDataContext context) : base(context) { }


    }
}
